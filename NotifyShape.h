//
//  NotifyShape.h
//  AmbientNotify
//
//  Created by Eric Betts on 11/4/08.
//  Copyright 2008 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <OpenGL/OpenGL.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#include <stdlib.h>
#import <CommonCrypto/CommonDigest.h>
#import <ScreenSaver/ScreenSaver.h>


#define MIN_X -13
#define MAX_X 13

#define MIN_Y -8
#define MAX_Y 8

#define MAX_Z -19
#define MIN_Z -21



@interface NotifyShape : NSObject {

    GLfloat r, g, b;
    GLfloat x, y, z;
    GLfloat x_direction, y_direction, z_direction;
    GLfloat x_rot, y_rot, z_rot;
    GLfloat radius;
    GLfloat deg;
    NSString *appName;
    //GLfloat angle, Hypot, dist, xp, yp, slowdown;
	GLfloat rotation, rotation_delta, initial_z;
    NSInteger count;
    ScreenSaverDefaults *defaults;
}
- (id)init;
- (id)initWithColor:(NSColor *)color;
- (id)initWithString:(NSString *)str;
- (void)DrawSelf:(NSOpenGLView *)glview;
- (void)Update;
- (GLfloat)Reciprocal:(GLfloat) n;
- (NSString*) md5:(NSString *)str;
- (NSColor*)colorWithHexColorString:(NSString*)inColorString;

@property (readwrite, assign) NSInteger count;

@end
