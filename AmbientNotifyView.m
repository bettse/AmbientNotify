//
//  AmbientNotifyView.m
//  AmbientNotify
//
//  Created by Eric Betts on 11/4/08.
//  Copyright (c) 2008, Eric Betts. All rights reserved.
//

#import "AmbientNotifyView.h"


@implementation AmbientNotifyView

- (id)initWithFrame:(NSRect)frame isPreview:(BOOL)isPreview
{
    self = [super initWithFrame:frame isPreview:isPreview];
    if (self) {

        NSOpenGLPixelFormatAttribute attributes[] = {
            NSOpenGLPFAAccelerated,
            /*NSOpenGLPFAFullScreen,*/
            NSOpenGLPFADoubleBuffer,
            NSOpenGLPFABackingStore,
            NSOpenGLPFADepthSize, 16,
            /*NSOpenGLPFAMinimumPolicy,*/
            /*NSOpenGLPFAClosestPolicy,*/
            0
        };
        NSOpenGLPixelFormat *format;
        
        defaults = [ScreenSaverDefaults defaultsForModuleWithName:@"org.ericbetts.AmbientNotify"];
        
        // Register our default values
        [defaults registerDefaults:[NSDictionary dictionaryWithObjectsAndKeys:
                                    @"NO", @"Orbit",
                                    @"YES", @"Bounce",
                                    nil]];

        
        format = [[[NSOpenGLPixelFormat alloc] initWithAttributes:attributes] autorelease ];
        glView = [[MyOpenGLView alloc] initWithFrame:NSZeroRect pixelFormat:format];
        if (!glView) { NSLog( @"Couldn't initialize OpenGL view." ); [self autorelease]; return nil; }
        [self addSubview:glView];
        [self setUpOpenGL];
        NotifyDict = [[NSMutableDictionary alloc] initWithCapacity:5];

        //setup NSConnection
        serverConnection=[[NSConnection new] autorelease];
        [serverConnection setRootObject:self];
        if([serverConnection registerName:@"org.ericbetts.ambientgrowl"] == NO){
            NSLog(@"Couldn't register Ambient Notify.\n");
        }else{
            NSLog(@"Successfully registered Ambient Notify.\n");
            SystemNotifyShape *system = [[SystemNotifyShape alloc] init];
            [NotifyDict setObject:system forKey:@"System"];
            [system release];
            
            [serverConnection setReplyTimeout:1];
            [serverConnection setRequestTimeout:1];
        }

        

        [self setAnimationTimeInterval:1/30.0];
    }
    return self;
}

- (void)startAnimation
{
    [super startAnimation];

}

- (void)stopAnimation
{
    [super stopAnimation];
}

- (void)drawRect:(NSRect)rect
{
    [super drawRect:rect];

    //make this the context and clear away the old stuff
    [[glView openGLContext] makeCurrentContext];
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


    //give each ball a chance to draw itself
    for (NotifyShape *element in [NotifyDict objectEnumerator]) {
        [element DrawSelf:glView];
    }


    //glFlush is for single buffered mode, flushBuffer for doublebuffer
    //glFlush();
    [[glView openGLContext] flushBuffer];

    return;

}

- (void)animateOneFrame
{

    //update the position of the notification balls
    for (NotifyShape *element in [NotifyDict objectEnumerator]) {
        [element Update];      //<-- polymorphism!!!!
    }

    [self setNeedsDisplay:YES];
    return;
}

- (BOOL)hasConfigureSheet
{
    return YES;
}

- (NSWindow*)configureSheet
{

    if (!configSheet)
    {
        if (![NSBundle loadNibNamed:@"ConfigureSheet" owner:self])
        {
            NSLog( @"Failed to load configure sheet." );
            NSBeep();
        }
    }
    if ([defaults boolForKey:@"Orbit"]){
        //Orbit is currently the top element
        [visOptions selectCellAtRow:0 column:0];
    } else if ([defaults boolForKey:@"Bounce"]){
        [visOptions selectCellAtRow:1 column:0];
    }
    return configSheet;
}

- (IBAction)cancelClick:(id)sender
{
    [[NSApplication sharedApplication] endSheet:configSheet];
}


- (IBAction) okClick: (id)sender
{

    // Update our defaults
    NSButton *selCell = [visOptions selectedCell];
    NSLog(@"Selected cell is %@ (%ld,%ld)", [selCell title], [visOptions selectedRow], [visOptions selectedColumn]);
    if([[selCell title] isEqualToString:@"Orbit"]){
        [defaults setBool:YES forKey:@"Orbit"];
        [defaults setBool:NO forKey:@"Bounce"];
    }else{
        [defaults setBool:NO forKey:@"Orbit"];
        [defaults setBool:YES forKey:@"Bounce"];
    }
    // Save the settings to disk
    [defaults synchronize];

    // Close the sheet
    [[NSApplication sharedApplication] endSheet:configSheet];
}


- (void)setUpOpenGL
{
    [[glView openGLContext] makeCurrentContext];

    // set vertical sych to refresh rate (removes tearing)
    GLint vals = 0x01;
    [[glView openGLContext] setValues:&vals forParameter:NSOpenGLCPSwapInterval];

	glShadeModel( GL_SMOOTH );
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

    //enable lighting
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHT2);

    // Create light components
    GLfloat ambientLight[] = { 0.2f, 0.2f, 0.2f, 1.0f };
    GLfloat diffuseLight[] = { 0.8f, 0.8f, 0.8, 1.0f };
    GLfloat specularLight[] = { 0.5f, 0.5f, 0.5f, 1.0f };
    GLfloat position0[] = { -1.5f, 1.0f, -4.0f, 1.0f };
    GLfloat position1[] = { 1.5f, 1.0f, -4.0f, 1.0f };
    GLfloat position2[] = { 0.0f, 3.0f, -10.0f, 1.0f };

    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT0, GL_POSITION, position0);

    glLightfv(GL_LIGHT1, GL_AMBIENT, ambientLight);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuseLight);
    glLightfv(GL_LIGHT1, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT1, GL_POSITION, position1);

    glLightfv(GL_LIGHT2, GL_AMBIENT, ambientLight);
    glLightfv(GL_LIGHT2, GL_DIFFUSE, diffuseLight);
    glLightfv(GL_LIGHT2, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT2, GL_POSITION, position2);


    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

}

+(BOOL)performGammaFade{
    return NO; //sharp transition into and out of screensaver
}

- (void)setFrameSize:(NSSize)newSize
{
    [super setFrameSize:newSize];
    [glView setFrameSize:newSize];

    [[glView openGLContext] makeCurrentContext];
    glViewport(0, 0, (GLsizei)newSize.width, (GLsizei)newSize.height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45.0f, (GLfloat)newSize.width / (GLfloat)newSize.height, 1.0f, 50.0f);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    [[glView openGLContext] update];

}

- (void)dealloc
{
    [serverConnection registerName:nil];
	[glView removeFromSuperview];
	[glView release];
    [NotifyDict release];
	[super dealloc];
}



-(oneway void)request:(NSString *)request {
    NSLog(@"Received request: %@", request);
    NotifyShape *ns;

    if ([defaults boolForKey:@"Orbit"]){
        //Look for existing notification object
        ns = [NotifyDict objectForKey:request];
        if(ns){
            ns.count++;
        }else{
            //Create new object
            ns = [[NotifyShape alloc] initWithString:request];
            [NotifyDict setObject:ns forKey:request];
            [ns release];
        }
    } else if ([defaults boolForKey:@"Bounce"]){
        ns = [[NotifyShape alloc] initWithString:request];
        [NotifyDict setObject:ns forKey:[NSDate date]];
        [ns release];

    }

}


@end
