//
//  SystemNotifyShape.h
//  AmbientNotify
//
//  Created by Eric Betts on 11/7/08.
//  Copyright 2008 Eric Betts. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NotifyShape.h"
#import <OpenGL/OpenGL.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>
#import "MyOpenGLView.h"

Boolean onlyoneexists;

@interface SystemNotifyShape : NotifyShape {
}

@end
