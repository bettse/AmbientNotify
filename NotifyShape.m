//
//  NotifyShape.m
//  AmbientNotify
//
//  Created by Eric Betts on 11/4/08.
//  Copyright 2008 Eric Betts. All rights reserved.
//

#import "NotifyShape.h"



@implementation NotifyShape

@synthesize count;

- (id)init
{
    return [self initWithColor:[NSColor purpleColor]];
}


-(id)initWithColor:(NSColor *)color
{
    if (self) {
        count = 1;
        r = [color redComponent];
        g = [color greenComponent];
        b = [color blueComponent];
		radius = 0.15f;


		rotation = 0.0f;
        initial_z = -20;

		x_rot = 0;//arc4random() % 2;
		y_rot = 0;//arc4random() % 2;
		z_rot = 1;//arc4random() % 2;
		//GLfloat range = (1.0f + (arc4random() % 10000 / 1000.0));
        rotation_delta = (1.0f + (arc4random() % 3000 / 1000.0));


		x = (1.0f + (arc4random() % 3000 / 1000.0));
        y = (1.0f + (arc4random() % 3000 / 1000.0));
        z = (initial_z + (arc4random() % 3000 / 1000.0));

        x_direction = (0.1f + (arc4random() % 20 / 1000.0));
        y_direction = (0.1f + (arc4random() % 20 / 1000.0));
        z_direction = (0.1f + (arc4random() % 20 / 1000.0));

        defaults = [ScreenSaverDefaults defaultsForModuleWithName:@"org.ericbetts.AmbientNotify"];


    }
    return self;


}

-(id)initWithString:(NSString *)str
{
    appName = str;
    NSString * hex = [[self md5:appName] substringWithRange:NSMakeRange(0, 6)];
    NSColor * color = [self colorWithHexColorString:hex];
    return [self initWithColor:color];
}

- (void)DrawSelf:(NSOpenGLView *)glView
{

    [[glView openGLContext] makeCurrentContext];


    glLoadIdentity();
    glPushMatrix();

    if ([defaults boolForKey:@"Orbit"]){
        glTranslatef(0, 0, initial_z);
    	glRotatef(rotation, x_rot, y_rot, z_rot);
    }

    glTranslatef(x, y, z);


    GLUquadric *quadric = NULL;

    glColor3f(r, g, b);

    quadric = gluNewQuadric();
    gluQuadricTexture( quadric, GL_TRUE );
    gluQuadricDrawStyle( quadric, GLU_FILL );
    gluSphere( quadric, radius * count, 48, 24 );

    gluDeleteQuadric(quadric);
    quadric = NULL;

    glPopMatrix();

    return;
}

-(void)Update
{
    
    if ([defaults boolForKey:@"Orbit"]){
        rotation += rotation_delta * count;
        if(rotation >= 360)
            rotation = 0;
    } else if ([defaults boolForKey:@"Bounce"]){
        x += x_direction;
        y += y_direction;
        z += z_direction;
        if( (x > MAX_X && x_direction > 0) || (x < MIN_X && x_direction < 0))
            x_direction *= -1;
        if( (y > MAX_Y && y_direction > 0) || (y < MIN_Y && y_direction < 0))
            y_direction *= -1;
        if( (z > MAX_Z && z_direction > 0) || (z < MIN_Z && z_direction < 0))
            z_direction *= -1;
     
    }
    return;

}

-(GLfloat)Reciprocal:(GLfloat) n
{
    double close = .1f;
    if (n < close && n >= 0) // prevent devision of 0
    {
        return 1/close;
    }
    else if (n > -close && n < 0) // prevent devision of 0
    {
        return 1/-close;
    }
    return 1/n;
}

- (NSString*) md5:(NSString *)str;
{
    const char *cStr = [str UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, strlen(cStr), result );
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]
            ];
}

- (NSColor*)colorWithHexColorString:(NSString*)inColorString
{
	NSColor* result    = nil;
	unsigned colorCode = 0;
	unsigned char redByte, greenByte, blueByte;

	if (nil != inColorString)
	{
		NSScanner* scanner = [NSScanner scannerWithString:inColorString];
		(void) [scanner scanHexInt:&colorCode]; // ignore error
	}
	redByte   = (unsigned char)(colorCode >> 16);
	greenByte = (unsigned char)(colorCode >> 8);
	blueByte  = (unsigned char)(colorCode);     // masks off high bits

	result = [NSColor
		          colorWithCalibratedRed:(CGFloat)redByte    / 0xff
		          green:(CGFloat)greenByte / 0xff
		          blue:(CGFloat)blueByte   / 0xff
		          alpha:1.0];
	return result;
}

@end
