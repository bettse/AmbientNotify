//
//  AmbientNotifyView.h
//  AmbientNotify
//
//  Created by Eric Betts on 11/4/08.
//  Copyright (c) 2008, Eric Betts. All rights reserved.
//  The basis for the OpenGL in this screensaver adapter from http://www.cocoadevcentral.com/articles/000089.php

#import <ScreenSaver/ScreenSaver.h>
#import <OpenGL/OpenGL.h>
#import <OpenGL/gl.h>
#import <OpenGL/glu.h>

#import "MyOpenGLView.h"
#import "NotifyShape.h"
#import "SystemNotifyShape.h"

@interface AmbientNotifyView : ScreenSaverView
{
    MyOpenGLView *glView;
    NSMutableDictionary *NotifyDict;
    int i; //counter to lower number of times each class is checked for new notifications
    NSConnection *serverConnection;
    IBOutlet id configSheet;
    IBOutlet NSMatrix *visOptions;
    ScreenSaverDefaults *defaults;
}
-(void)setUpOpenGL;

-(oneway void)request:(NSString *)request;
-(IBAction)cancelClick:(id)sender;
-(IBAction)okClick:(id)sender;

@end
