//
//  SystemNotifyShape.m
//  AmbientNotify
//
//  Created by Eric Betts on 11/7/08.
//  Copyright 2008 Eric Betts. All rights reserved.
//

#import "SystemNotifyShape.h"


@implementation SystemNotifyShape // all the methods are overridden to provide a different shape and rotation
- (id)init
{
    if (self) {
        r = 1.0f; g = 1.0f; b = 0.0f; //yellow
        x = 0.0f;
        y = 0.0f;
        z = -10.0f;
        radius = 0.15f;
        x_direction = y_direction = z_direction = 1; // 1  or -1 to determine direction
        onlyoneexists = YES;
        rotation = 0.0f;
    }
    return self;
}


- (void)DrawSelf:(NSOpenGLView *)glView
{
    [[glView openGLContext] makeCurrentContext];


    glLoadIdentity();
    glPushMatrix();


    glTranslatef(0, 0, z);
	//glRotatef(rotation, 0.0f, 0.0f, 1.0f);
    //glTranslatef(x, y, z);



    GLUquadric *quadric = NULL;
    glColorMaterial(GL_FRONT, GL_COLOR_INDEXES);
    glColor3f(r, g, b);

    quadric = gluNewQuadric();
    gluQuadricTexture( quadric, GL_TRUE );
    gluQuadricDrawStyle( quadric, GLU_FILL );
    gluSphere( quadric, radius, 48, 24 );

    gluDeleteQuadric(quadric);
    quadric = NULL;

    glPopMatrix();

    return;
}


-(void)Update{

	rotation += 10.0f;
	if(rotation >= 360)
		rotation = 0;

    return;

}

@end
